var clientesObtenidos;

function gestionClientes()
{
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //console.log(request.responseText);
            clientesObtenidos = request.responseText;
            
            procesarClientes();
        }
    };

    request.open("GET", url, true);
    request.send();
}

//ContactName, Country, City

function procesarClientes()
{
    var JSONClientes = JSON.parse(clientesObtenidos);
    //alert(JSONClientes.value[0].ProductName);

    var divTabla = document.getElementById("divTabla");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    // La funcion rapida
    /*$.each(JSONClientes.value, function (key, value) {
        console.log(value.ProductName);
        var nuevaFila = document.createElement("tr");
        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = value.ProductName;
        var columnaPrecio = document.createElement("td");
        columnaPrecio.innerText = value.UnitPrice;
        var columnaStock = document.createElement("td");
        columnaStock.innerText = value.UnitsInStock;

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaPrecio);
        nuevaFila.appendChild(columnaStock);

        tbody.appendChild(nuevaFila);
    });*/

    for (var i = 0; i < JSONClientes.value.length; i++)
    {
        //console.log(JSONClientes.value[i].ProductName);
        var nuevaFila = document.createElement("tr");
        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONClientes.value[i].ContactName;
        var columnaCountry = document.createElement("td");

        switch (JSONClientes.value[i].Country)
        {
            case "UK":
                columnaCountry.innerHTML ="<img src='https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png'>";
                break;
            default:
                columnaCountry.innerHTML ="<img src='https://www.countries-ofthe-world.com/flags-normal/flag-of-" + JSONClientes.value[i].Country + ".png'>";
        }

        var columnaCity = document.createElement("td");
        columnaCity.innerText = JSONClientes.value[i].City;

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaCity);
        nuevaFila.appendChild(columnaCountry);

        tbody.appendChild(nuevaFila);
    }

    tabla.appendChild(tbody);
    divTabla.innerHTML = '';
    divTabla.appendChild(tabla);
}